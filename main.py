# -*- coding: iso8859-15 -*-

import multiprocessing
import os
import requests
import shlex
import subprocess
import sys
import threading
import time

from datetime import datetime
from time import mktime

from settings import SNIFF_INTERFACE, BASE_DIR
from utils.system import get_platform, update_hostname, get_uptime

sys.path.append(BASE_DIR)

from additional import install_dataplicity, request_header, update_blacklist_from_s3
from utils.common import (
    is_rpi, logger, config, cik_config, client, get_serial, get_users,
    reboot_device, save_cik_config_sync,
    MANUFACTURE_URL, EXCLUDE_URL
)
from utils.net import start_monitor, check_internet_connection
from utils.sql import RawData
from wifi_sniffer.sniffer import WiFiSniffer, mac_parser

UPLOAD_INTERVAL = config['UploadInterval']


class EyeConic(threading.Thread):

    _sniff_queue = multiprocessing.Queue()
    # _data_queue = multiprocessing.Queue()
    _raw_queue = multiprocessing.Queue()

    def __init__(self):
        super(EyeConic, self).__init__()
        self.sniffer = WiFiSniffer(out_queue=self._sniff_queue, raw_out_queue=self._raw_queue)
        # self.filter = VisitFilter(in_queue=self._sniff_queue, out_queue=self._data_queue)

    def _monitor_thread(self):
        while True:
            thread_list = threading.enumerate()
            logger.info(thread_list)
            is_uploading = False
            for item in thread_list:
                if item.name == 'upload_raw':
                    is_uploading = True
            if not is_uploading:
                logger.error('>>>>> No upload thread. Rebooting ')
                reboot_device()
            time.sleep(5 * 60)

    def run(self):

        if is_rpi() and SNIFF_INTERFACE not in os.listdir('/sys/class/net'):
            start_monitor()

        if is_rpi():
            self.sniffer.start()

        if config['UploadRaw']:
            threading.Thread(target=self.upload_raw_data, name='upload_raw').start()
            threading.Thread(target=self._monitor_thread, name='thread_mon').start()

    def upload_raw_data(self):
        while True:
            s_time = time.time()
            records = []
            while self._raw_queue.qsize() > 0:
                try:
                    records.append(self._raw_queue.get())
                except Exception as e:
                    client.captureException()
                    logger.error('Failed to get raw record from the queue - {}'.format(e))
                    break
            if records:
                record_count = RawData.select().count()
                if record_count > config['Limit']['MaxRecord']:
                    logger.warn('>>>>> Removing old 1000 records')
                    to_delete = RawData.select().order_by(RawData.id.asc()).limit(1000)
                    n_deleted = RawData.delete().where(RawData.id << to_delete).execute()

                # save raw data to sqlite db if there is no internet connection
                if not check_internet_connection():
                    logger.warn('>>>>> No internet. Saving data to local db')
                    for item in records:
                        obj = RawData(org=item['org'], mac=item['mac'], distance=item['distance'],
                                      ts=item['ts'], rssi=item['rssi'], datetime=datetime.fromtimestamp(item['ts']),
                                      channel=item.get('channel', None))
                        obj.save()
                    time.sleep(UPLOAD_INTERVAL)
                    continue

                if ('HOST' in config) and config['HOST']['Enabled']:
                    query = None
                    old_records = []
                    if RawData.select().count():
                        query = RawData.select().order_by(RawData.id.asc()).limit(100)
                        for item in query:
                            old_records.append(dict(org=item.org, mac=item.mac, distance=item.distance,
                                                    ts=item.ts, rssi=item.rssi))

                    try:
                        old_records.extend(records)
                        for record in old_records:
                            record['manufacturer'] = record['org']
                            record['timestamp'] = record['ts']

                        res = requests.post(
                            '%s/%s' % (config['HOST']['URL'], config['HOST']['DataEndpoint']),
                            json={"data": old_records},
                            headers=request_header()
                        )

                        if res.status_code != 200:
                            logger.error(res.text)
                            for item in records:
                                obj = RawData(org=item['org'], mac=item['mac'], distance=item['distance'],
                                              ts=item['ts'], rssi=item['rssi'], channel=item.get('channel', None),
                                              datetime=datetime.fromtimestamp(item['ts']))
                                obj.save()
                        else:
                            res_json = res.json()
                            if res_json['code'] != 'success':
                                raise ValueError('Requested invalid data')

                            if query and query.count():
                                n_deleted = RawData.delete().where(RawData.id << query).execute()

                            logger.info('Uploaded raw data')
                    except Exception as e:
                        client.captureException()
                        logger.error(str(e))
                        for item in records:
                            obj = RawData(org=item['org'], mac=item['mac'], distance=item['distance'],
                                          ts=item['ts'], rssi=item['rssi'], datetime=datetime.fromtimestamp(item['ts']),
                                          channel=item.get('channel', None))
                            obj.save()
                logger.debug(str(records))
            elapsed = time.time() - s_time
            time.sleep(max(0, UPLOAD_INTERVAL - elapsed))


if __name__ == '__main__':

    logger.info('========== Starting EYECONIC Device ==========')

    if os.getuid() != 0:
        exit("You need to have root privileges to run this service.\n Please try with `sudo`")

    if get_platform()['hostname'] != 'EYECONIC-{}'.format(get_serial()):
        logger.warning('Hostname is not set, updating...')
        update_hostname()
        logger.info('Now, rebooting...')
        os.system('reboot')

    subprocess.call(shlex.split("git config user.name pi"))
    subprocess.call(shlex.split("git config user.email pi_%s@eyeconic.tv" % get_serial()))

    # update reboot status as executed if uptime is less than 20 seconds
    if cik_config.get('reboot') and cik_config['reboot']['received'] and get_uptime() < 120:
        cik_config['reboot']['executed'] = True

    # load CIK (token)
    if cik_config['enabled'] and not cik_config['token']:
        response = requests.post('%s/%s' % (config['HOST']['URL'], config['HOST']['RegisterEndpoint']),
                                 json={"serial": get_serial()})
        if response.status_code != 200:
            pass
            # os.system('reboot')
        else:
            obj = response.json()
            if obj['code'] == 'success':
                cik_config['token'] = obj['data']['token']
                n_deleted = RawData.delete().execute()
                save_cik_config_sync(os.path.join(BASE_DIR, 'cik.json'), cik_config)

    while True:
        if not check_internet_connection():
            logger.warn('>>>>> No internet. Check connection after 5 mins')
            time.sleep(5 * 60)
        else:
            break
    # check for oui list
    oui_last_time = cik_config.get('oui_latest')
    blacklist_last_time = cik_config.get('blacklist_latest')

    if all([oui_last_time, blacklist_last_time,
            oui_last_time > time.time() - 300, blacklist_last_time > time.time() - 300]):
        pass
    else:
        update_cik_config = False
        need_reboot = False
        # check OUI list
        res = requests.head(MANUFACTURE_URL)
        modified = res.headers.get('last-modified')
        modified = time.strptime(modified, "%a, %d %b %Y %H:%M:%S %Z")
        latest_ts = mktime(modified)
        if not oui_last_time or oui_last_time < latest_ts:
            cik_config['oui_latest'] = latest_ts
            mac_parser.update()
            logger.info('>>> Updating OUI database...')
            update_cik_config = True

        # check exclusion list
        res = requests.head(EXCLUDE_URL)
        modified = res.headers.get('last-modified')
        modified = time.strptime(modified, "%a, %d %b %Y %H:%M:%S %Z")
        latest_ts = mktime(modified)
        if not blacklist_last_time or blacklist_last_time < latest_ts:
            cik_config['blacklist_latest'] = latest_ts
            logger.info('>>> Updating exclusion list ...')
            update_blacklist_from_s3()
            need_reboot = True
            update_cik_config = True

        if not cik_config.get('version'):
            cik_config['version'] = config['Device']['version']
            update_cik_config = True

        if update_cik_config:
            save_cik_config_sync(os.path.join(BASE_DIR, 'cik.json'), cik_config)
            if need_reboot:
                reboot_device(False)

    # check for dataplicity
    if config['Device']['remote'] and not cik_config.get('remote'):
        res = install_dataplicity()
        if res and 'dataplicity' in get_users():
            cik_config['remote'] = True
            cik_config['version'] = '1.1'
            save_cik_config_sync(os.path.join(BASE_DIR, 'cik.json'), cik_config)
            reboot_device(False)

    if 'version' in cik_config and cik_config['version'] == '1.1':
        try:
            subprocess.check_call(shlex.split('bash %s/scripts/overlayfs/setup.sh' % BASE_DIR))
            cik_config['version'] = '1.2'
            save_cik_config_sync(os.path.join(BASE_DIR, 'cik.json'), cik_config)
            reboot_device(False)
        except Exception as e:
            logger.error('Failed to install overlayfs')

    sniffer = EyeConic()

    sniffer.start()

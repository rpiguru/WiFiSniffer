# -*- coding: iso8859-15 -*-

import numpy as np


class RangeStateEstimate:
    """
    Estimates initial range measurement and updates this range based on the Kalman filter.
    """
    def __init__(self, x_init, t_init, P_init, Q):
        """
        Constructor
        :param x_init: (numpy.array, shape=(3,1)) Initial state estimate including velocity and acceleration
         - [[range],[range_dot],[range_dot_dot]].
        :param t_init: (int) Initial timestamp.
        :param P_init: (numpy.matrix, shape=(3,3)) Initial state covariance matrix (error in state estimate).
        :param Q: (numpy.matrix, shape=(3,3)) Process noise covariance (process noise).
        """
        self.t = t_init
        self.x = x_init
        self.P = P_init
        self.Q = Q

        # H - Measurement matrix used to map state estimate and state covariance to dimensionality of new measurements.
        self.H = np.matrix([[1, 0, 0]])

        # Lambda function to produce F, the state estimate and state covariance propagation matrix
        # So x(t) = F*x(t-dt)
        # And P(t) = F*P(t-dt)*F'
        # Where F = self.build_f(dt)
        self.build_f = lambda dt: np.matrix([[1, dt, .5 * dt ** 2], [0, 1, dt], [0, 0, 1]])

    def propagate(self, dt):
        """
        Propagate the state estimate and covariance forward in time
        :param dt: (int) (Time of new state estimate) - (time of current state estimate)
        :return self.t + dt, new_x, new_P: Tuple containing updated timestamp, new state estimate at updated timestamp,
        and new state covariance estimate at updated timestamp
        """
        # Get F for dt
        F = self.build_f(dt)

        # Propagate state estimate to current_time + dt
        new_x = F * self.x

        # Propagate state covariance to current_time + dt
        new_P = F * self.P * F.transpose() + self.Q

        return self.t + dt, new_x, new_P

    def update(self, t, z, R, propagated_x=None, propagated_P=None):
        """
        Incorporate a new measurement to refine the propagated state estimate and state covariance.
        NB: This does not update the current state of an instance of this class.
            The retain_state method has been provided to retain states produced from this update method.
        :param t: (int) Timestamp of new measurement
        :param z: (float) New range measurement
        :param R: (float) Squared error of measurement
        :param propagated_x: (numpy.array, shape=(3,1), optional) Propagated state estimate.
        If not passed, current state estimate will be propagated forward to the time of the new measurement.
        :param propagated_P: (numpy.matrix, shape=(3,3), optional) Propagated state covariance.
        If not passed, current state covariance will be propagated forward to the time of the new measurement.
        :return: new_t, x_new, P_new: Tuple containing updated timestamp, new state estimate at updated timestamp,
        and new state covariance estimate at updated timestamp
        """
        new_t = 0
        # If propagated state estimate not passed, propagate current state
        if propagated_x is None and t is not None:
            new_t, propagated_x, propagated_P = self.propagate(t - self.t)

        # Obtain difference between the new measurement and the propagated state estimate
        delta_x = z - self.H * propagated_x

        # Construct error matrix (1x1 in this case) incorporating R (the squared measurement error) and
        # the terms of the propagated state covariance that correspond to the measured variables
        # (in this case range only).
        S = R + self.H * propagated_P * self.H.transpose()

        # Kalman update

        # Compute Kalman gain, the factor by which the new measurements contribution to the updated state will be scaled
        # 1) Measurements with low error => significant contribution
        #       (e.g. if error is zero, the updated state estimate will become the new measurement)
        # 2) Measurements with high error => diminished contribution
        #       (e.g. if error is very large, the updated state estimate will ignore the new measurement)
        K = propagated_P * self.H.transpose() * np.linalg.inv(S)

        # Add the difference between the new measurement and the propagated state estimate to the propagated state,
        # scaled by the Kalman gain
        x_new = propagated_x + K * delta_x

        # Add the error from the new measurement to the propagated state covariance, scaled by the Kalman gain
        P_new = propagated_P - K * S * K.transpose()

        return new_t, x_new, P_new

    def retain_state(self, t, x, P):
        """
        Convenience method to retain a previously calculated state.
        :param t: Timestamp to use as time of state estimate
        :param x: (numpy.array, shape=(3,1)) State estimate
        :param P: (numpy.matrix, shape=(3,3)) State covariance
        :return:
        """
        self.t = t
        self.x = x
        self.P = P

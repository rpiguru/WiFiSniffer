# -*- coding: iso8859-15 -*-

import time
import multiprocessing
import datetime
import numpy

from utils.common import logger, config
from visit_filter.classifier import VisitClassifier


class VisitFilter(multiprocessing.Process):

    _b_stop = multiprocessing.Event()
    _in_queue = None
    _out_queue = None

    def __init__(self, in_queue=None, out_queue=None):
        super(VisitFilter, self).__init__()
        self._in_queue = in_queue
        self._out_queue = out_queue
        self._b_stop.clear()

    def run(self):
        params = config["Filtering"]
        while True:
            if self._b_stop.is_set():
                time.sleep(.1)
                continue

            if self._in_queue.qsize() > 0:
                m = self._in_queue.get()
                m['duration'] = round(m['latest_ts'] - m['start_ts'], 4)
                # Skip this set of measurements and do not record a visit if:
                # 1) The time span of measurements is less than the minimum configured for a visit
                # 2) the number of measurements is less than the minimum configured for a visit
                if m['duration'] < params["MinVisitTime"] or len(m['record']) < params["MinVisitMeasurementCount"]:
                    logger.info('{} has too short appearance time({}) or samples({}), removed this.'.format(
                        m['mac'], m['duration'], len(m['record'])))
                    continue
                elif m['duration'] >= params['MaxVisitTime']:
                    logger.info('{} has too long time({}), which seems to be wrong, '
                                'removed this.'.format(m['mac'], m['duration']))
                    continue

                # Create new VisitClassifier instance with parameters that define the minimum number of
                # measurements and the minimum time span of a visit
                vc = VisitClassifier(min_count=params["MinVisitMeasurementCount"],
                                     min_visit_time_sec=params["MinVisitTime"])

                # Identify visits that occur in the set of RSSI measurements.
                # Here multiple visits may be identified if a device exits and reenters a venue,
                # however only one visit will be recorded for each device.
                current_visit_df = vc.find_visits(measurement_list=m['record'],
                                                  max_venue_visit_range=params["MaxEntryCertainRange"],
                                                  max_visit_dt=params["VisitBreakMaxDT"])

                # Record a single visit if any visits are found, regardless of how many and update record in mongodb
                if any(current_visit_df.visit >= 1):
                    # Duration should be the sum of row's timedelta which has certain distance.
                    m['duration'] = current_visit_df[
                        current_visit_df['filtered_range'] <= params["MaxEntryCertainRange"]]['dt'].sum()

                    if numpy.isnan(m['duration']):
                        m['duration'] = 0

                    r = dict(
                        start_time=datetime.datetime.fromtimestamp(m['start_ts']),
                        end_time=datetime.datetime.fromtimestamp(m['latest_ts']),
                        filtered_range=[round(d, 2) for d in current_visit_df['filtered_range']],
                        rssi=list(current_visit_df['rssi']),
                        ts=list(current_visit_df['ts']),
                        mac=m['mac'],
                        org=m['org'],
                        rssi_avg=round(current_visit_df['rssi'].mean(), 2),
                        range_avg=round(current_visit_df['filtered_range'].mean(), 2),
                        duration=m['duration']
                    )
                    self._out_queue.put(r)
                    logger.info('>>>>> Filtered record --- {}\n'.format(r))
                else:
                    logger.info('{} does not meet requirements'.format(m['mac']))

            time.sleep(.1)

    def pause(self):
        self._b_stop.set()

    def resume(self):
        self._b_stop.clear()

#!/usr/bin/env bash

echo "========== Starting Installation =========="

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt-get install -y \
    firmware-linux-free \
    libpcap-dev \
    supervisor \
    ufw \
    python \
    libpython-dev \
    python-pip \
    python-pcapy \


sudo pip install -U pip
sudo pip install -r requirements.txt

echo "Installing supervisor service..."
sudo cp ${cur_dir}/conf/projectdiscover.conf /etc/supervisor/conf.d/
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl restart all

echo "Remove the splash screen and pi branding"
sudo echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo systemctl mask plymouth-start.service
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/" /boot/cmdline.txt

echo "Changing the hostname..."
sn="$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2)"
hostname="EYECONIC-${sn:8:8}"
echo "${hostname}" | sudo tee /etc/hostname
printf "127.0.0.1\t${hostname}\n" | sudo tee -a /etc/hosts

# Update System Locale
sudo sed -i -- "s/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/" /etc/locale.gen
sudo dpkg-reconfigure tzdata

echo "All done!"

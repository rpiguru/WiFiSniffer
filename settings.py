# -*- coding: iso8859-15 -*-

# ============================= General Configuration ================================
import os

DEBUG = False

SNIFF_INTERFACE = 'mon.wlan0'
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

try:
    from local_settings import *
except ImportError:
    pass

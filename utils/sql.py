import os

from datetime import datetime
from peewee import *

from settings import BASE_DIR

db = SqliteDatabase(os.path.join(BASE_DIR, 'tmp.db'))


class RawData(Model):
    org = CharField()
    mac = CharField(max_length=30)
    distance = FloatField()
    ts = IntegerField()
    channel = IntegerField(null=True)
    rssi = FloatField()
    datetime = DateTimeField()

    class Meta:
        database = db


db.connect()
db.create_tables([RawData])

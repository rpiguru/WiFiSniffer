# -*- coding: iso8859-15 -*-

import httplib
import netifaces
import os
import psutil
import requests
import socket
import shlex
import subprocess
import time

from settings import DEBUG, SNIFF_INTERFACE
from utils.common import is_rpi, logger, get_users


def start_monitor():
    """
    Create a monitoring interface and start.
    :return:
    """
    # Get physical #
    logger.info("Preparing monitor")

    # change wifi adapter to monitor mode #
    # subprocess.check_call(shlex.split('ifconfig wlan1 down'))
    # subprocess.check_call(shlex.split('iwconfig wlan1 mode monitor'))
    # subprocess.check_call(shlex.split('ifconfig wlan1 up'))
    # time.sleep(1)
    # return

    # use built-in wifi chip to add monitor interface #
    pipe = os.popen('iw dev wlan0 info | grep wiphy')
    sudo_user = 'pi' if 'pi' in get_users() else 'eyeconic'
    data = pipe.read().strip()
    pipe.close()
    num = 0
    if not data:
        logger.info('No wifi adapter')
        try:
            subprocess.check_call(shlex.split('rmmod brcmfmac'))
        except Exception as e:
            pass
        logger.info("Unload wifi adapter")
        subprocess.check_call(shlex.split('insmod /home/%s/projectdiscover/conf/brcmfmac.ko' % sudo_user))
        logger.info("Loaded wifi adapter")
        subprocess.check_call(shlex.split('init 6'))
    else:
        num = data.split()[1]

    e = None
    try:
        subprocess.check_call(shlex.split('iw phy phy{} interface add {} type monitor'.format(num, SNIFF_INTERFACE)))
        logger.info("Added Monitor")
    except subprocess.CalledProcessError as e:
        logger.error('Failed to add monitor', e.returncode)

    if e and e.returncode == 161:
        logger.info("Fixing wifi adapter issue")
        try:
            subprocess.check_call(shlex.split('rmmod brcmfmac'))
            logger.info("Unload wifi adapter")
            subprocess.check_call(shlex.split('insmod /home/%s/projectdiscover/conf/brcmfmac.ko' % sudo_user))
            logger.info("Loaded wifi adapter")
            subprocess.check_call(shlex.split('init 6'))
        except subprocess.CalledProcessError as e:
            logger.error('Failed to reset monitor', e.returncode)
    os.system("ifconfig {} up".format(SNIFF_INTERFACE))
    logger.info('Started monitor')
    time.sleep(1)


def get_current_wifi_info():
    """
    Get the detailed information of the wifi AP.
    :return:
    """
    if is_rpi():
        pipe = os.popen('iwgetid -r')
        ap = pipe.read().strip()
        pipe.close()
        if ap == '':
            return None, None
        else:
            try:
                p = subprocess.Popen('iwconfig | grep "Access Point"', shell=True, stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                output, error = p.communicate()
                p.wait()
                line = output.decode('utf-8').splitlines()[-1].strip()
                mac = line.split('Access Point: ')[1].split()[0]
                return ap, mac
            except (ValueError, IndexError, ArithmeticError, TypeError):
                return None, None
    ap = 'Testing AP'
    mac = '11:22:33:44:55:66'
    return ap, mac


def check_internet_connection(url='www.google.com', timeout=3):
    """
    Check internet connection
        It will be faster to just make a HEAD request so no HTML will be fetched.
        Also I am sure google would like it better this way :)
    """
    conn = httplib.HTTPConnection(url, timeout=timeout)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except socket.error:
        conn.close()
        return False


def get_mac_address(interface='wlan0'):
    """
    Retrieve MAC address of the wifi interface
    :return:
    """
    if is_rpi():
        try:
            info = netifaces.ifaddresses(interface)[netifaces.AF_LINK]
            return info[0]['addr']
        except Exception as e:
            if not DEBUG:
                logger.error('Failed to get MAC address of {} - {}'.format(interface, e))
                return
    return '00:11:22:33:44:55'


def connect_to_ap(ssid='', pwd=''):
    """
    Connect to AP and return new assigned IP address
    :param ssid:
    :param pwd:
    :return: Return None when failed, otherwise return (IP, is_restored)
    """
    # Disconnect from the current AP:
    ap, mac = get_current_wifi_info()
    if ap:
        print('Disconnecting {}...'.format(ap))
        p = subprocess.Popen('sudo nmcli d disconnect wlan0', shell=True, preexec_fn=os.setpgrp,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        print('Result => Out: {}, Error: {}'.format(stdout, stderr))
        p.wait()

    time.sleep(.5)

    logger.info('Net: Connecting to {}...'.format(ssid))
    p = subprocess.Popen('sudo nmcli dev wifi connect "{}" password "{}"'.format(ssid, pwd),
                         shell=True, preexec_fn=os.setpgrp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    logger.info('Net: Result => Out: {}, Error: {}'.format(stdout, stderr))
    p.wait()

    return get_ip_address('wlan0')


def get_ip_address(ifname='wlan0'):
    """
    Get local IP address of the interface
    :param ifname: interface name such as wlan0, eth0, etc
    :return: If not on RPi, returns the IP address of LAN
    """
    if not is_rpi():
        ifname = 'enp3s0'
    try:
        return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    except Exception as e:
        logger.error('Net: Failed to get IP address of {}, reason: {}'.format(ifname, e))


def get_local_ip():
    pipe = os.popen("hostname -I|cut -d' ' -f1")
    data = pipe.read().strip()
    pipe.close()
    return data


def get_external_ip():
    """
    Get external IP address of this machine
    """
    try:
        return str(requests.get('http://v4.ident.me').text)
    except Exception as e:
        logger.error('Failed to get external IP, seems like no-internet? - {}'.format(e))


def get_geo_location():
    """
    Retrieve geo-location from http://ipinfo.io/json
        sample result:
            {   u'city': u'',
                u'country': u'AU',
                u'hostname': u'gen-119-17-162-118.ptr4.otw.net.au',
                u'ip': u'119.17.162.118',
                u'loc': u'-27.4710,153.0243',
                u'org': u'AS9268 Over the Wire Pty Ltd',
                u'region': u'Queensland'
            }
    """
    url = 'http://ipinfo.io/json'
    try:
        data = requests.get(url).json()
        # TODO: If latitude & longitude are not returned, retry with other service
        return data
    except Exception as e:
        logger.exception('Failed to get geolocation, seems like no-internet? - {}'.format(e))


def get_lat_loc():
    data = get_geo_location()
    try:
        val = str(data['loc']).split(',')
        return val
    except (ValueError, AttributeError, TypeError):
        pass


def get_traffic():
    """
    Get the upload/download speed
    """
    global measure_time
    global traffic_data
    if traffic_data is None:
        traffic_data = psutil.net_io_counters(pernic=False)
        measure_time = time.time()
        return

    duration = time.time() - measure_time
    new_data = psutil.net_io_counters(pernic=False)
    try:
        upload = (new_data.bytes_sent - traffic_data.bytes_sent) / duration
        download = (new_data.bytes_recv - traffic_data.bytes_recv) / duration
        traffic_data = new_data
        measure_time = time.time()
        return {'upload': convert_to_bps(upload), 'download': convert_to_bps(download)}
    except Exception as e:
        logger.exception('Failed to get network speed: {}'.format(e))


def convert_to_bps(speed):
    """
    Convert numeric speed value to bps expression.
    :param speed:
    :return:
    """
    if speed > 1024 * 1024 / 10:
        return '{} MB/s'.format(round(speed / 1024 / 1024, 1))
    elif speed > 1024 / 10:
        return '{} KB/s'.format(round(speed / 1024, 1))
    else:
        return '{} B/s'.format(round(speed))

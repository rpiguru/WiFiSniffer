# -*- coding: iso8859-15 -*-
import json
import logging.config
import os
import pwd
import shlex
import shutil
import subprocess

from raven import Client

_cur_dir = os.path.dirname(os.path.realpath(__file__))
# S3 files
EXCLUDE_URL = "https://s3.amazonaws.com/projectdiscover/proximiti/exclude.json"
MANUFACTURE_URL = "https://s3.amazonaws.com/projectdiscover/proximiti/oui/manufacture.data"

client = Client('https://a31ba91d72e44f30961b1edd0bc31d51:f3d1a52af19348abb257baa44947eb36@sentry.io/1198571')

# logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,

    'formatters': {
        'console': {
            'format': '[%(asctime)s][%(threadName)s][%(levelname)s] '
                      '%(filename)s:%(funcName)s:%(lineno)d | %(message)s',
            'datefmt': '%Y-%m-%dT%H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'console'
        },
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': 'https://a31ba91d72e44f30961b1edd0bc31d51:f3d1a52af19348abb257baa44947eb36@sentry.io/1198571',
        },
        'info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/EyeConicInfo.log',
            'maxBytes': 1024 * 1024 * 3,
            'backupCount': 3,
            'formatter': 'console'
        },
        'warning': {
            'level': 'WARNING',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/EyeConicWarn.log',
            'maxBytes': 1024 * 1024 * 3,
            'backupCount': 5,
            'formatter': 'console'
        },
        'rotate': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/EyeConic.log',
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
            'formatter': 'console'
        }
    },

    'loggers': {
        'EyeConic': {
            'handlers': ['console', 'sentry', 'info', 'warning', 'rotate'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'root': {
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}

logging.config.dictConfig(LOGGING)
logger = logging.getLogger('EyeConic')


def is_rpi():
    return 'arm' in os.uname()[4]


def get_distance_from_rssi(rssi):
    return round(10. ** ((-51. - rssi) / (10. * 2.)), 2)


def get_serial():
    """
    Retrieve the unique serial number of RPi.
    :return:
    """
    if is_rpi():
        try:
            data = open('/proc/cpuinfo', 'r').read()
            for line in data.splitlines():
                if line[0:6] == 'Serial':
                    return line[10:26].lstrip('0')
        except Exception as e:
            print('Failed to get serial number - {}'.format(e))
    else:
        return '12345678'
    return None


def get_users():
    return [p[0] for p in pwd.getpwall()]


def get_service_status(name):
    """
    Checks whether given service exists
    :param name: service name
    :return:
    """
    pipe = os.popen('service --status-all|grep %s' % name)
    res = pipe.read().strip()
    pipe.close()
    return name in res


def reboot_device(do_sync=True):
    if do_sync and get_service_status('saveoverlays'):
        subprocess.check_call(shlex.split('service saveoverlays sync'))
    subprocess.check_call(shlex.split('init 6'))


def save_cik_config_sync(config_path, content):
    with open(config_path, 'w') as f:
        f.write(json.dumps(content, indent=4))
    if get_service_status('saveoverlays'):
        subprocess.check_call(shlex.split('service saveoverlays sync'))


# load config files
json_file = os.path.join(_cur_dir, os.pardir, 'config_{}.json'.format(get_serial()))
if not os.path.exists(json_file):
    logger.warning('JSON config file not found, restoring default...')
    shutil.copy(os.path.join(_cur_dir, os.pardir, 'config.json'), json_file)

cik_file = os.path.join(_cur_dir, os.pardir, 'cik.json')
exclude_file = os.path.join(_cur_dir, os.pardir, 'exclude.json')

config = json.loads(open(json_file).read())
cik_config = json.loads(open(cik_file).read())

if os.path.exists(exclude_file):
    exclude_config = json.loads(open(exclude_file).read())
else:
    exclude_config = {
        "MAC": config['Sniffing']['Exclusion']['MAC'],
        "manufacture": config['Sniffing']['Exclusion']['Manufacture']
    }
    save_cik_config_sync(exclude_file, exclude_config)


if __name__ == '__main__':
    print(is_rpi())

from collections import namedtuple
import re
import io
import os
from urllib2 import urlopen
from urllib2 import URLError
from StringIO import StringIO
from utils.common import logger, MANUFACTURE_URL

# Vendor tuple
Vendor = namedtuple('Vendor', ['manufacture', 'manufacture_long', 'comment'])

_cur_dir = os.path.dirname(os.path.realpath(__file__))


class MacParser(object):
    """
        Class that contains a parser for Wireshark's OUI database.

        Optimized for quick lookup performance by reading the entire file into memory on
        initialization. Maps ranges of MAC addresses to manufacturers and comments (descriptions).
        Contains full support for netmasks and other strange things in the database.
        See https://www.wireshark.org/tools/oui-lookup.html
    """
    _masks = {}
    
    def __init__(self, manufacture_name="manufacture.data", update=False):
        self._manufacture_name = os.path.join(_cur_dir, manufacture_name)
        if update:
            self.update()
        else:
            self.refresh()

    def refresh(self, manufacture_name=None):
        """
            Refresh/reload manufacture.data database. Call this when manufacture.data file is updated.
        """
        if not manufacture_name:
            manufacture_name = self._manufacture_name

        with io.open(manufacture_name, "r", encoding="utf-8") as read_file:
            manufacture_file = StringIO(read_file.read())
        self._masks = {}

        # Build mask -> result dict
        for line in manufacture_file:
            try:
                line = line.strip()
                if not line or line[0] == "#":
                    continue
                line = line.replace("\t\t", "\t")
                fields = [field.strip() for field in line.split("\t")]

                parts = fields[0].split("/")
                mac_str = _strip_mac(parts[0])
                mac_int = _get_mac_int(mac_str)
                mask = _bits_left(mac_str)

                # Specification includes mask
                if len(parts) > 1:
                    mask_spec = 48 - int(parts[1])
                    if mask_spec > mask:
                        mask = mask_spec

                comment = fields[3].strip("#").strip() if len(fields) > 3 else None
                long_name = fields[2] if len(fields) > 2 else None

                self._masks[(mask, mac_int >> mask)] = Vendor(
                    manufacture=fields[1], manufacture_long=long_name, comment=comment)
            except Exception as e:
                print("Couldn't parse line", line)
                raise e
        manufacture_file.close()
        logger.info('OUI Refresh finished')

    def update(self, manufacture_url=None, manufacture_name=None, refresh=True):
        """
            Update the Wireshark OUI database to the latest version.
            :param refresh:
            :param manufacture_name:
            :param manufacture_url:
        """
        if not manufacture_url:
            manufacture_url = MANUFACTURE_URL
        if not manufacture_name:
            manufacture_name = self._manufacture_name

        # Retrieve the new database
        try:
            response = urlopen(manufacture_url)
        except URLError:
            raise URLError("Failed downloading OUI database")

        # Parse the response
        if response.code is 200:
            with open(manufacture_name, "wb") as write_file:
                write_file.write(response.read())
            if refresh:
                self.refresh(manufacture_name)
        else:
            err = "{0} {1}".format(response.code, response.msg)
            raise URLError("Failed downloading database: {0}".format(err))

        logger.info("Downloaded OUI database")
        response.close()

    def search(self, mac, maximum=1):
        """Search for multiple Vendor tuples possibly matching a MAC address.
            :param mac: MAC address in standard format.
            :type mac: str
            :param maximum: Maximum results to return.
            :type maximum: int
            :return: List of Vendor named tuples containing (manufacture.data, comment), with closest result first.
                     May be empty if no results found.
            :rtype: list
        """
        vendors = []
        if maximum <= 0:
            return vendors
        mac_str = _strip_mac(mac)
        mac_int = _get_mac_int(mac_str)

        # If the user only gave us X bits, check X bits. No partial matching!
        for mask in range(_bits_left(mac_str), 48):
            result = self._masks.get((mask, mac_int >> mask))
            if result:
                vendors.append(result)
                if len(vendors) >= maximum:
                    break
        return vendors

    def get_all(self, mac):
        """
            Get a Vendor tuple containing (manufacture.data, comment) from a MAC address.
            :param mac: MAC address in standard format.
            :return: Vendor namedtuple containing (manufacture.data, comment).
                     Either or both may be None if not found.
            :rtype: Vendor
        """
        vendors = self.search(mac)
        if len(vendors) == 0:
            return Vendor(manufacture=None, manufacture_long=None, comment=None)
        return vendors[0]

    def get_manufacture(self, mac):
        """
            Returns manufacturer from a MAC address.
        """
        return self.get_all(mac).manufacture

    def get_manufacture_long(self, mac):
        """
            Returns manufacturer long name from a MAC address.
        """
        return self.get_all(mac).manufacture_long

    def get_comment(self, mac):
        """
            Returns comment from a MAC address.
        """
        return self.get_all(mac).comment


def _get_mac_int(mac_str):
    """
    Gets the integer representation of a stripped mac string
    :param mac_str:
    :return:
    """
    try:
        # Fill in missing bits with zeroes
        return int(mac_str, 16) << _bits_left(mac_str)
    except ValueError:
        raise ValueError("Could not parse MAC: {0}".format(mac_str))


def _strip_mac(mac):
    """
    Strips the MAC address of '-', ':', and '.' characters
    :param mac:
    :return:
    """
    pattern = re.compile(r"[-:.]")
    return pattern.sub("", mac)


def _bits_left(mac_str):
    """
    Gets the number of bits left in a mac string
    :param mac_str:
    :return:
    """
    return 48 - 4 * len(mac_str)


if __name__ == "__main__":
    p = MacParser()
    p.update()
    print(p.get_all('6C:FD:B9:9A:E2:B7'))

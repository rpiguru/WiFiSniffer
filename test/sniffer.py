import time
import multiprocessing

from wifi_sniffer.sniffer import WiFiSniffer


if __name__ == '__main__':
    _q = multiprocessing.Queue()
    w = WiFiSniffer(out_queue=_q)
    w.start()
    while True:
        if _q.qsize() > 0:
            print(_q.get())
        time.sleep(.1)

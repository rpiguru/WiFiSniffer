#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
 echo "This script must be run as root"
 exit 1
fi

# Given a filename, a regex pattern to match and a string:
# If found, no change, else append file with string on new line.
append1() {
	grep $2 $1 >/dev/null
	if [ $? -ne 0 ]; then
		# Not found; append on new line (silently)
		echo $3 | sudo tee -a $1 >/dev/null
	fi
}

# Given a filename, a regex pattern to match and a string:
# If found, no change, else append space + string to last line --
# this is used for the single-line /boot/cmdline.txt file.
append2() {
	grep $2 $1 >/dev/null
	if [ $? -ne 0 ]; then
		# Not found; insert in file before EOF
		sed -i "s/\'/ $3/g" $1 >/dev/null
	fi
}

# Given a filename, a regex pattern to match and a replacement string:
# Replace string if found, else no change.
# (# $1 = filename, $2 = pattern to match, $3 = replacement)
replace() {
	grep $2 $1 >/dev/null
	if [ $? -eq 0 ]; then
		# Pattern found; replace in file
		sed -i "s/$2/$3/g" $1 >/dev/null
	fi
}

# Given a filename, a regex pattern to match and a replacement string:
# If found, perform replacement, else append file w/replacement on new line.
replaceAppend() {
	grep $2 $1 >/dev/null
	if [ $? -eq 0 ]; then
		# Pattern found; replace in file
		sed -i "s/$2/$3/g" $1 >/dev/null
	else
		# Not found; append on new line (silently)
		echo $3 | sudo tee -a $1 >/dev/null
	fi
}


echo "===== Removing unwanted packages and make file system readonly ====="
apt-get remove -y --purge triggerhappy logrotate dphys-swapfile fake-hwclock dbus
apt-get -y autoremove --purge
append2 /boot/cmdline.txt fastboot fastboot
append2 /boot/cmdline.txt noswap noswap

echo "===== Setting up Clock Sync... ====="
apt-get install -y ntpdate

NTP_CMD="\/usr\/sbin\/ntpdate -b us.pool.ntp.org"
sed -i "s/^exit 0/${NTP_CMD}\\nexit 0/g" /etc/rc.local >/dev/null


echo "===== Installing busybox-syslogd... ====="
# You will be able to see the log using `logread` command.
apt-get -y install busybox-syslogd
dpkg --purge rsyslog

# Make SSH work
replaceAppend /etc/ssh/sshd_config "^.*UsePrivilegeSeparation.*$" "UsePrivilegeSeparation no"
# bbro method (not working in Jessie?):
#rmdir /var/run/sshd
#ln -s /tmp /var/run/sshd

# Change spool permissions in var.conf (rondie/Margaret fix)
replace /usr/lib/tmpfiles.d/var.conf "spool\s*0755" "spool 1777"

echo "Finished cleaning, please reboot now!"
exit 0

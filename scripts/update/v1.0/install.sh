#!/usr/bin/env bash
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
base_dir=/home/eyeconic/projectdiscover/

cp ${cur_dir}/config.json ${base_dir}/config.json
rm ${base_dir}/config_*.json


cd ${base_dir}/scripts/overlayfs
bash setup.sh

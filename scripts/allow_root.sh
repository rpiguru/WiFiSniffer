#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
 echo "This script must be run as root"
 exit 1
fi

# allow root login
sed -i '/#PermitRootLogin prohibit-password/c\PermitRootLogin yes' /etc/ssh/sshd_config
/etc/init.d/ssh restart

# change root password as eyeconic
echo "root:rB3cQ8t0akBDmkbNLF#RVRw8" | chpasswd

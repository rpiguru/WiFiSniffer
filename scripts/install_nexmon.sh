#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
 echo "This script must be run as root"
 exit 1
fi

apt-get update

echo "======== Enable Monitor Mode on WiFi ========"

apt-get install -y git libgmp3-dev gawk qpdf bison flex make

cd ~
git clone https://github.com/seemoo-lab/nexmon.git
cd nexmon/buildtools/isl-0.10
./configure
make
make install
ln -s /usr/local/lib/libisl.so /usr/lib/arm-linux-gnueabihf/libisl.so.10

cd ~/nexmon
source setup_env.sh
touch DISABLE_STATISTICS
make

cd patches/bcm43430a1/7_45_41_46/nexmon/
make
make backup-firmware
make install-firmware

cd ~/nexmon/utilities/nexutil/
make && make install

# Install new modified driver to be loaded after reboot
file_path="$( modinfo brcmfmac | grep filename | awk -v N=$2 '{print $2}' )"
echo "Original Driver: ${file_path}"
mv ${file_path} ${file_path}.orig
cp ~/nexmon/patches/bcm43430a1/7_45_41_46/nexmon/brcmfmac_kernel49/brcmfmac.ko ${file_path}
cp ~/nexmon/patches/bcm43430a1/7_45_41_46/nexmon/brcmfmac_kernel49/brcmfmac.ko /home/pi/projectdiscover/conf/
depmod -a
cd ~
rm -rf nexmon

#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
 echo "This script must be run as root"
 exit 1
fi

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

# Set up fuse and mount script
apt-get update
apt-get install -y fuse lsof

# Install services
chmod a+rx ${cur_dir}/saveoverlays ${cur_dir}/mount_overlay ${cur_dir}/rootro
cp ${cur_dir}/mount_overlay /usr/local/bin
cp ${cur_dir}/saveoverlays /etc/init.d/
cp ${cur_dir}/rootro /usr/local/bin/
ln -s ${cur_dir}/rootro /usr/local/bin/rootrw
cp ${cur_dir}/syncoverlayfs.service /lib/systemd/system/
systemctl disable dphys-swapfile

for D in /home /var
do
  cp -r ${D} ${D}_org
  rm -rf ${D}
  cd ${D}_org
  find . | cpio -pdum ${D}_stage
  mkdir -v ${D} ${D}_rw
done

# Backup and recovery
# cp -v /boot/cmdline.txt /boot/cmdline.txt-orig
# cp -v /etc/fstab /etc/fstab-orig
# cd /var
# tar --exclude=swap -cf /var-orig.tar .

systemctl daemon-reload
systemctl enable syncoverlayfs.service

# Update fstab
partuuid="$( blkid | grep /dev/mmcblk0p1 | cut -d ' ' -f 5 )"
partuuid=${partuuid:10:8}
rm /etc/fstab
touch /etc/fstab
cat > /etc/fstab <<EOF
proc            /proc           proc    defaults    0 0
PARTUUID=${partuuid}-01  /boot  vfat    ro          0 2
PARTUUID=${partuuid}-02  /      ext4    ro,noatime  0 1
mount_overlay   /var            fuse    nofail,defaults 0 0
mount_overlay   /home           fuse    nofail,defaults 0 0
none            /tmp            tmpfs   defaults    0 0
EOF

mount /home
mount /var

# Make micro SD card read-only.
# change /boot/cmdline.txt to include noswap fastboot ro
# dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=PARTUUID=b5ab69f9-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait noswap fastboot ro
sed -i "s/\'/ ro/g" /boot/cmdline.txt

echo "=== Installed overlayfs successfully! Please execute 'sudo service saveoverlays sync' if you want to synchronize. ==="
echo "========== Now, rebooting... =========="
#reboot

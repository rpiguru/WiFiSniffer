#!/usr/bin/env bash
if [[ $EUID -ne 0 ]]; then
 echo "This script must be run as root"
 exit 1
fi

echo "===== Installing watchdog... ====="
apt-get install -y watchdog

# Add to /etc/modules, update watchdog config file
echo "bcm2835_wdt" | tee -a /etc/modules
sed -i -- "s/#watchdog-device/watchdog-device/" /etc/watchdog.conf
sed -i -- "s/#max-load-1/max-load-1/" /etc/watchdog.conf
sed -i -- "s/#min-memory/min-memory/" /etc/watchdog.conf
echo "pidfile = /var/run/supervisord.pid" | tee -a /etc/watchdog.conf
echo "RuntimeWatchdogSec=10s" | tee -a /etc/systemd/system.conf
echo "ShutdownWatchdogSec=10min" | tee -a /etc/systemd/system.conf
sed -i -- "s/$/ bcm2835_wdt.heartbeat=10/" /boot/cmdline.txt

cat > /etc/udev/rules.d/60-watchdog.rules << EOF
KERNEL=="watchdog", MODE="0666"
EOF

# Start watchdog at system start and start right away
apt-get install -y insserv
insserv watchdog; /etc/init.d/watchdog start

systemctl enable watchdog
systemctl start watchdog

# Set up automatic reboot in sysctl.conf
echo "kernel.panic = 10" | tee -a /etc/sysctl.conf

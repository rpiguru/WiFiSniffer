#!/usr/bin/env bash
if [[ $EUID -ne 0 ]]; then
 echo "This script must be run as root"
 exit 1
fi

echo "===== Installing watchdog... ====="
apt-get update
apt-get install -y ufw

ufw default deny incoming
ufw allow 80/tcp
ufw allow 443/tcp
ufw deny 22
ufw enable

sed -i '/ufw-before-.*icmp/s/ACCEPT/DROP/g' /etc/ufw/before.rules
// Referenced from modmypi.com how to change the default account username and password

# nano /etc/ssh/sshd_config
# PermitRootLogin yes and restart

sudo passwd root    # to set root password

logout              # to logout from pi user

usermod -l <newname> pi     # change the pi user to newname

usermod -m -d /home/newname newname     # change the pi directory to reflect new username
usermod -aG sudo newname
# logout and login back with newname

passwd  # to change password

# change cmd execution behavior
sudo visudo

# kill all pi user thread

# check sudo privileges
sudo -k
sudo apt update

sudo passwd -l root   # lock root password
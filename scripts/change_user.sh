#!/usr/bin/env bash
if [[ $EUID -ne 0 ]]; then
 echo "This script must be run as root"
 exit 1
fi

# change default password
echo "pi:rB3cQ8t0akBDmkbNLF#RVRw8" | chpasswd

# kill pi usage
pkill -u pi pid
pkill -9 -u pi

# change login username
usermod -l eyeconic pi

# change home dir
usermod -m -d /home/eyeconic eyeconic

# add new user to sudo group
usermod -aG sudo eyeconic

# no password prompt for sudo group
echo "%sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# path update on supervisor
sed -i 's#/home/pi/projectdiscover#\/home/eyeconic/projectdiscover#g' /etc/supervisor/conf.d/projectdiscover.conf

sed -i '/PermitRootLogin yes/c\#PermitRootLogin prohibit-password' /etc/ssh/sshd_config
/etc/init.d/ssh restart
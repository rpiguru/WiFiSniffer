# Passive WiFi Sniffer on RPi

## Installation

- Download Raspbian **Stretch Lite** from [here](http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2018-03-14/2018-03-13-raspbian-stretch-lite.zip) and install.

- Clone this repo

        sudo apt-get install raspberrypi-kernel-headers
        sudo apt-get update
        sudo apt-get install -y git
        cd /home/pi
        git clone https://github.com/eyeconictv/projectdiscover.git

- Enable monitor mode of the wifi chip (No need on RPi 2):

        sudo -i

    Move to `scripts` directory and execute the following shell script:
        
        cd /home/pi/projectdiscover/scripts
        bash install_nexmon.sh
    
- Now, execute the installation script!
        
        su pi
        cd ~/projectdiscover
        bash install.sh
        sudo bash scripts/install_watchdog.sh
        sudo bash scripts/install_ufw.sh
    Service will be started automatically along with **watchdog** .

- Remove redundant packages:
 
        cd /home/pi/projectdiscover/scripts
        sudo bash cleaner.sh
        
- Run `sudo raspi-config` to configure the localization and keyboard options:
   - Change the Keyboard Layout
   - Choose: Generic 101-key PC
   - Choose: English (US) - May be under Other
        - Continue with Defaults

- Change default user:

    - Enable root login
        ```
        cd /home/pi/projectdiscover/scripts
        sudo bash allow_root.sh
        ```
    - Logout from pi user, login with root user (using `eyeconic` user password)
    - Change default `pi` user and password, disable root login
        ```
        cd /home/pi/projectdiscover/scripts
        bash change_user.sh
        ```
    - Logout from root user and login with `eyeconic` user
        ```
        su - eyeconic
        sudo service supervisor restart
        ```
        
~~- Install **overlayfs** for stability~~
        ```
        cd /home/eyeconic/projectdiscover/scripts/overlayfs
        sudo bash setup.sh
        sudo reboot
        ```     
        


        
        


## How to update the original file system

    # Stop the supervisor and watchdog service
    sudo service supervisor stop
    sudo service watchdog stop
    
    # Change file system to rw mode.
    rootrw
    
    # Do anything you want here! git pull on /home_org/eyeconic, etc
    git pull on /home_org/eyeconic/projectdiscover

    # In case of using ftp, get zip file to /home_stage/pi. change contents in /home_org from /home_stage
    
    # Now, synchronize and make it readonly again.
    sudo service saveoverlays sync # this is just in case the next command fail. Can happen when updates restart services
    rootro
    sudo init 6

## How to find the serial number

    cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2

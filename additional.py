import json
import os
import requests
import shlex
import shutil
import subprocess

from settings import BASE_DIR
from utils.net import get_local_ip, get_external_ip, check_internet_connection
from utils.common import (
    cik_config, client, config, get_serial, get_users, get_service_status, logger, reboot_device, save_cik_config_sync,
    EXCLUDE_URL
)
from utils.system import get_cpu_temperature_c, get_last_commit_hash, get_uptime, get_platform


def update_device(file_url):
    if not file_url:
        return

    try:
        if 'update' in cik_config and cik_config['update']['received'] and cik_config['update']['updated']:
            res = requests.post(
                '%s/%s' % (config['HOST']['URL'], config['HOST']['FlagEndpoint']),
                json={"status": ['received_update']},
                headers=request_header()
            )
            if res.status_code == 200 and res.json()['code'] == 'success':
                cik_config['update'] = {
                    "received": False,
                    "updated": False
                }
                with open(os.path.join(BASE_DIR, 'cik.json'), 'w') as f:
                    f.write(json.dumps(cik_config, indent=4))
                reboot_device()
            else:
                raise ValueError('>>>>> Received error message')
        else:
            logger.info('>>>>> Starting update')
            version = cik_config.get('version', '1.0')
            # Sync saveoverlayfs first, update source, etc
            # subprocess.check_call(shlex.split('git add -A'))
            # subprocess.check_call(shlex.split('git commit -am "v%s"' % version))
            last_hash = get_last_commit_hash()
            cik_config['update'] = {
                "received": True,
                "updated": False
            }
            save_cik_config_sync(os.path.join(BASE_DIR, 'cik.json'), cik_config)

            download_path = '%s/scripts/install.tar.gz' % BASE_DIR
            update_path = os.path.join(BASE_DIR, 'scripts', 'update')
            if os.path.exists(download_path):
                os.remove(download_path)
            subprocess.check_call(shlex.split('wget -O %s "%s"' % (download_path, file_url)))
            if os.path.exists(update_path):
                shutil.rmtree(update_path)
            subprocess.check_call(shlex.split('tar -xf %s -C %s/scripts/' % (download_path, BASE_DIR)))
            new_version = [f for f in os.listdir(update_path) if os.path.isfile(os.path.join(update_path, f))]
            if not new_version:
                raise ValueError('No version info in tar file')

            new_version = new_version[0].split('version')[1]
            if new_version == version:
                logger.info('Already up-to-date')
            else:
                subprocess.check_call(shlex.split('bash %s/v%s/install.sh' % (update_path, version)))

            # TODO git hash value
            cik_config['update'] = {
                "received": True,
                "updated": True
            }
            cik_config['version'] = new_version
            save_cik_config_sync(os.path.join(BASE_DIR, 'cik.json'), cik_config)

            res = requests.post(
                '%s/%s' % (config['HOST']['URL'], config['HOST']['FlagEndpoint']),
                json={"status": ['received_update']},
                headers=request_header()
            )

            if res.status_code == 200 and res.json()['code'] == 'success':
                cik_config['update'] = {
                    "received": False,
                    "updated": False
                }

                if new_version == version:
                    save_cik_config_sync(os.path.join(BASE_DIR, 'cik.json'), cik_config)
                else:
                    with open(os.path.join(BASE_DIR, 'cik.json'), 'w') as f:
                        f.write(json.dumps(cik_config, indent=4))
                    reboot_device()
            else:
                raise ValueError('>>>>> Received error response')
    except Exception as e:
        client.captureException()
        logger.warning('>>>>> Failed to update device %s' % str(e))


def reboot_by_flag():
    reboot = cik_config.get('reboot', None)
    logger.info('>>>>> Rebooting')
    if reboot and reboot.get('received', None) and reboot.get('executed', None):
        res = requests.post(
            '%s/%s' % (config['HOST']['URL'], config['HOST']['FlagEndpoint']),
            json={"status": ['received_reboot']},
            headers=request_header()
        )
        if res.status_code == 200 and res.json()['code'] == 'success':
            cik_config['reboot'] = dict(received=False, executed=False)
            save_cik_config_sync(os.path.join(BASE_DIR, 'cik.json'), cik_config)
    else:
        cik_config['reboot'] = dict(received=True, executed=False)
        with open(os.path.join(BASE_DIR, 'cik.json'), 'w') as cik_file:
            cik_file.write(json.dumps(cik_config, indent=4))
        reboot_device()


def update_blacklist(file_url):
    if not file_url:
        return

    try:
        download_path = '%s/exclude.json' % BASE_DIR
        subprocess.check_call(shlex.split('wget -O %s "%s"' % (download_path, file_url)))

        res = requests.post(
            '%s/%s' % (config['HOST']['URL'], config['HOST']['FlagEndpoint']),
            json={"status": ['received_needUpdateList']},
            headers=request_header()
        )
        if res.status_code == 200 and res.json()['code'] == 'success':
            cik_config['update'] = {
                "received": False,
                "updated": False
            }
            with open(os.path.join(BASE_DIR, 'cik.json'), 'w') as f:
                f.write(json.dumps(cik_config, indent=4))
            reboot_device()
        else:
            raise ValueError('>>>>> Received error message')
        return True
    except Exception as e:
        client.captureException()
        logger.warning('>>>>> Failed to update blacklist %s' % str(e))
        return False


def update_blacklist_from_s3():
    try:
        download_path = '%s/exclude.json' % BASE_DIR
        subprocess.check_call(shlex.split('wget -O %s "%s"' % (download_path, EXCLUDE_URL)))
        return True
    except Exception as e:
        client.captureException()
        logger.warning('>>>>> Failed to update blacklist from s3: %s' % str(e))
        return False


def device_status():
    # TODO check whether config is consistent
    return {
        "serial": get_serial(),
        "temperature": get_cpu_temperature_c(),
        "public_ip": get_external_ip(),
        "local_ip": get_local_ip(),
        "version": cik_config['version'],
        "release_date": config['Device']['release_date'],
        "kernel": get_platform()['kernel'],
        "uptime": get_uptime()
    }


def request_header():
    return {
        'content-type': 'application/json',
        'Authorization': 'Bearer %s' % cik_config['token']
    }


def install_dataplicity():
    if not config['Device']['remote']:
        return False
    if not check_internet_connection():
        return False
    if 'dataplicity' in get_users():
        return True

    try:
        if get_service_status('saveoverlays'):
            subprocess.check_call('rootrw')
        ps = subprocess.Popen(('curl', 'https://www.dataplicity.com/m9nzev9e.py'), stdout=subprocess.PIPE)
        output = subprocess.check_call(('python',), stdin=ps.stdout)
        ps.wait()
        if get_service_status('saveoverlays'):
            subprocess.check_call(shlex.split('service saveoverlays sync'))
            subprocess.check_call('rootro')
        return True
    except Exception as e:
        logger.warn(e)
        return False


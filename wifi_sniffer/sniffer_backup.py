# -*- coding: iso8859-15 -*-
import os
import io
import multiprocessing
import pcapy
import dpkt.pcapng
import copy
import re
import shlex
import struct
import subprocess
import threading
import time
import numpy as np
import datetime

from Queue import Queue
from StringIO import StringIO

from settings import SNIFF_INTERFACE
from utils.common import get_distance_from_rssi, config, logger
from utils.manufacture.manufacture import MacParser

p = MacParser()


_cur_dir = os.path.dirname(os.path.realpath(__file__))


def _strip_mac(mac):
    """
    Strips the MAC address of '-', ':', and '.' characters
    :param mac:
    :return:
    """
    pattern = re.compile(r"[-:.]")
    return pattern.sub("", mac)


def get_oui_from_mac(mac):
    """
    parse mac address and look up the organization from the vendor octets
    :param mac:
    :return:
    """
    try:
        oui = p.get_manufacture_long(mac)
        if oui is None:
            oui = p.get_manufacture(mac)
        if oui not in {None, 'ORGANIZATIONNAME'}:
            return oui
    except ValueError as e:
        print('Failed to parse MAC address({}) - {}'.format(mac, e))


class WiFiSniffer(multiprocessing.Process):

    _cap = None
    _stop_event = threading.Event()

    _lock_raw = threading.RLock()
    _raw_data = {}
    _raw_queue = Queue()

    _buf = {}
    _lock = threading.RLock()

    _exclude_list = []

    def __init__(self, out_queue=None, raw_out_queue=None):
        super(WiFiSniffer, self).__init__()
        self._out_queue = out_queue
        self._raw_out_queue = raw_out_queue
        self._stop_event.clear()
        self._compose_exclude_list()

    def _compose_exclude_list(self):
        with io.open(os.path.join(_cur_dir, 'exclude.data'), "r", encoding="utf-8") as read_file:
            exclude_file = StringIO(read_file.read())
        for line in exclude_file:
            line_data = line.strip()
            if not line_data or line_data[0] == '#':
                continue
            self._exclude_list.append(_strip_mac(line_data.split()[0].split('/')[0]))

    def run(self):
        self._cap = pcapy.open_live(SNIFF_INTERFACE, 1514, 1, 0)
        # type mgt subtype probe-resp
        # self._cap.setfilter('type mgt subtype assoc-req')
        # self._cap.setfilter('subtype auth')
        # self._cap.setfilter('subtype probe-req or subtype assoc-req or subtype reassoc-req or subtype disassoc or subtype auth')  # ps-poll: 10, rts: 11
        # self._cap.setfilter('type ctl subtype rts')
        # self._cap.setfilter('subtype probe-req or subtype null or subtype auth')
        self._cap.setfilter('subtype null')
        threading.Thread(target=self._filter_records).start()
        # threading.Thread(target=self._channel_hop).start()
        threading.Thread(target=self._update_buf).start()
        threading.Thread(target=self._check_monitor).start()
        threading.Thread(target=self._extract_disappeared_devices).start()
        while True:
            if not self._stop_event.is_set():
                (header, pkt) = self._cap.next()
                if self._cap.datalink() == dpkt.pcapng.DLT_IEEE802_11_RADIO:
                    rtlen = struct.unpack('h', pkt[2:4])[0]
                    ftype = (ord(pkt[rtlen]) >> 2) & 3
                    stype = ord(pkt[rtlen]) >> 4
                    # possible pairs: (0, 4), (1, 11), (2, 4), (2, 8)
                    # (mgt probe-req), (ctl rts), (data null), (data qos-data)
                    # check if probe request
                    # ftype: wlan_type (mgt: 0, ctl: 1, data: 2),
                    # stype: subtype (assoc-req: 0, ) ( 4 : Probe request, 5: Probe response )
                    if stype >= 0:
                        rtap = pkt[:rtlen]
                        frame = pkt[rtlen:]
                        bssid = frame[10:16].encode('hex')
                        bssid = ':'.join([bssid[x:x + 2] for x in xrange(0, len(bssid), 2)])
                        mac = str(bssid).upper()
                        rssi = struct.unpack("b", rtap[-2:-1])[0]
                        # essid = ''
                        if len(frame) > 25:
                            if ord(frame[25]) > 0:
                                # essid = frame[26:26+ord(frame[25])]
                                is_my_client = False
                            else:
                                is_my_client = True
                        else:
                            is_my_client = False
                        if stype == 11:
                            is_my_client = True
                        if -50 < rssi < -30:     # Maximum RSSI is -30dBm(10cm)
                            logger.info('%s %s %s %s %s' % (len(frame), ftype, stype, mac, rssi))
                            if mac in config['Sniffing']['Exclusion']['MAC']:
                                continue
                            org = get_oui_from_mac(mac)
                            if org is None:         # <= #23
                                continue
                            if org == 'Raspberry Pi Foundation':
                                continue
                            if any(m in org for m in config['Sniffing']['Exclusion']['Manufacture']):
                                continue
                            if any(_strip_mac(mac).startswith(m) for m in self._exclude_list):
                                continue

                            record = dict(ts=header.getts()[0], rssi=rssi, org=org, stype=stype)
                            with self._lock_raw:
                                if mac in self._raw_data:
                                    self._raw_data[mac].append(record)
                                else:
                                    self._raw_data[mac] = [record]
            else:
                time.sleep(.1)

    def _filter_records(self):
        """
        Filter the records which are duplicated in a very short period of time.
        :return:
        """
        while True:
            if not self._stop_event.is_set():
                with self._lock_raw:
                    for mac, record in self._raw_data.items()[:]:
                        t_list = [r['ts'] for r in record]
                        r_list = [r['rssi'] for r in record]
                        if time.time() - max(t_list) < 1.:
                            continue
                        r = dict(mac=mac,
                                 ts=t_list[len(t_list) // 2],
                                 rssi=round(np.average(r_list), 2),
                                 org=record[0]['org'])
                        self._raw_queue.put(r)
                        self._raw_data.pop(mac)
            time.sleep(1)

    def _update_buf(self):
        """
        Pop raw data from the queue and update the local buf in memory
        :return:
        """
        while True:
            if not self._stop_event.is_set():
                while self._raw_queue.qsize() > 0:
                    r = self._raw_queue.get()
                    r['distance'] = get_distance_from_rssi(r['rssi'])
                    if r['distance'] < config['Sniffing']['MaxSniffDistance']:
                        if config['UploadRaw']:
                            self._raw_out_queue.put(copy.deepcopy(r))
                        mac = r.pop('mac')
                        print('\t{} : MAC: {}, {}'.format(datetime.datetime.now(), mac, r))
                        with self._lock:
                            if mac in self._buf:
                                self._buf[mac]['record'].append(r)
                                self._buf[mac]['latest_ts'] = r['ts']
                            else:
                                self._buf[mac] = {
                                    'mac': mac,
                                    'org': r['org'],
                                    'start_ts': r['ts'],
                                    'latest_ts': r['ts'],
                                    'record': [r],
                                }
            time.sleep(1)

    def _channel_hop(self):
        """
        Pop raw data from the queue and update the local buf in memory
        :return:
        """
        channel = 0
        while True:
            channel = (channel % 13) + 1
            if not self._stop_event.is_set():
                with self._lock:
                    subprocess.call(shlex.split("nexutil -I mon.wlan0 --chanspec=%d" % channel))
            time.sleep(3)

    def _check_monitor(self):
        """
        Check monitoring interface.
        :return:
        """
        while True:
            if not self._stop_event.is_set():
                with self._lock:
                    pipe = os.popen('iwlist channel | grep Frequency')
                    msg = pipe.read().strip()
                    pipe.close()
                    if 'Channel' not in msg:
                        logger.error('>>>>>>> Monitoring failed. Rebooting')
                        subprocess.check_call(shlex.split('init 6'))
            time.sleep(30 * 60)

    def _extract_disappeared_devices(self):
        """
        Extract "disappeared" devices and put to the output queue
        :return:
        """
        while True:
            if not self._stop_event.is_set():
                with self._lock:
                    for k, v in self._buf.items()[:]:
                        if time.time() - v['latest_ts'] > config["Filtering"]['DisappearanceTime']:
                            self._buf.pop(k)
                            self._out_queue.put(v)
            time.sleep(1)

    def pause(self):
        self._stop_event.set()

    def resume(self):
        self._stop_event.clear()


if __name__ == '__main__':
    _q = multiprocessing.Queue()
    w = WiFiSniffer(out_queue=_q)
    w.start()
    while True:
        if _q.qsize() > 0:
            print(_q.get())
        time.sleep(.1)

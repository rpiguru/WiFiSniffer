# -*- coding: iso8859-15 -*-
import copy
import datetime
import dpkt.pcapng
import io
import json
import multiprocessing
import numpy as np
import os
import pcapy
import re
import requests
import shlex
import struct
import subprocess
import threading
import time

from Queue import Queue
from StringIO import StringIO

from additional import device_status, reboot_by_flag, request_header, update_blacklist, update_device
from settings import SNIFF_INTERFACE
from utils.common import get_distance_from_rssi, config, cik_config, exclude_config, logger, reboot_device
from utils.manufacture.manufacture import MacParser
from utils.net import check_internet_connection

mac_parser = MacParser()

_cur_dir = os.path.dirname(os.path.realpath(__file__))


def _strip_mac(mac):
    """
    Strips the MAC address of '-', ':', and '.' characters
    :param mac:
    :return:
    """
    pattern = re.compile(r"[-:.]")
    return pattern.sub("", mac)


def get_oui_from_mac(mac):
    """
    parse mac address and look up the organization from the vendor octets
    :param mac:
    :return:
    """
    try:
        oui = mac_parser.get_manufacture_long(mac)
        if oui is None:
            oui = mac_parser.get_manufacture(mac)
        if oui not in {None, 'ORGANIZATIONNAME'}:
            return oui
    except ValueError as e:
        print('Failed to parse MAC address({}) - {}'.format(mac, e))


class WiFiSniffer(multiprocessing.Process):

    _cap = None
    _stop_event = threading.Event()

    _lock_raw = threading.RLock()
    _raw_data = {}
    _raw_queue = Queue()

    _buf = {}
    _lock = threading.RLock()

    _exclude_list = []

    def __init__(self, out_queue=None, raw_out_queue=None):
        super(WiFiSniffer, self).__init__()
        self._out_queue = out_queue
        self._raw_out_queue = raw_out_queue
        self._stop_event.clear()
        self._compose_exclude_list()

    def _compose_exclude_list(self):
        with io.open(os.path.join(_cur_dir, 'exclude.data'), "r", encoding="utf-8") as read_file:
            exclude_file = StringIO(read_file.read())
        for line in exclude_file:
            line_data = line.strip()
            if not line_data or line_data[0] == '#':
                continue
            self._exclude_list.append(_strip_mac(line_data.split()[0].split('/')[0]))

    def run(self):
        self._cap = pcapy.open_live(SNIFF_INTERFACE, 1514, 1, 0)
        # type mgt subtype probe-resp
        self._cap.setfilter('subtype probe-req or subtype null')
        threading.Thread(target=self._filter_records, name='filter_rec').start()
        threading.Thread(target=self._update_buf, name='update_buf').start()
        threading.Thread(target=self._check_monitor, name='check_mon').start()
        threading.Thread(target=self._ping_server, name='ping').start()
        threading.Thread(target=self._monitor_thread, name='thread_mon').start()
        # threading.Thread(target=self._channel_hop).start()
        # threading.Thread(target=self._extract_disappeared_devices).start()
        while True:
            if not self._stop_event.is_set():
                (header, pkt) = self._cap.next()
                if self._cap.datalink() == dpkt.pcapng.DLT_IEEE802_11_RADIO:
                    rtlen = struct.unpack('h', pkt[2:4])[0]
                    ftype = (ord(pkt[rtlen]) >> 2) & 3
                    stype = ord(pkt[rtlen]) >> 4
                    # check if probe request
                    # stype: subtype ( 4 : Probe request, 5: Probe response )
                    # frame length is different depending on whether device is associated to AP
                    if (ftype == 0 and stype == 0) or stype == 4:
                        rtap = pkt[:rtlen]
                        frame = pkt[rtlen:]
                        bssid = frame[10:16].encode('hex')
                        bssid = ':'.join([bssid[x:x + 2] for x in xrange(0, len(bssid), 2)])
                        mac = str(bssid).upper()
                        rssi = struct.unpack("b", rtap[-2:-1])[0]
                        if not rssi:
                            rssi = struct.unpack("b", rtap[-4:-3])[0]
                        # essid = ''
                        if len(frame) > 25:
                            if ord(frame[25]) > 0:
                                # essid = frame[26:26+ord(frame[25])]
                                is_my_client = False
                            else:
                                is_my_client = True
                        else:
                            is_my_client = False
                        if rssi < -30:     # Maximum RSSI is -30dBm(10cm)
                            if mac in exclude_config['MAC']:
                                continue
                            org = get_oui_from_mac(mac)
                            if org is None:         # <= #23
                                continue
                            if any(m in org for m in exclude_config['manufacture']):
                                continue
                            if any(_strip_mac(mac).startswith(m) for m in self._exclude_list):
                                continue

                            channel = 0
                            pipe = os.popen('iwlist %s channel | grep Frequency' % SNIFF_INTERFACE)
                            msg = pipe.read().strip()
                            pipe.close()
                            if 'Channel' in msg:
                                channel = int(msg.split('Channel')[1].strip('( )'))
                            record = dict(ts=header.getts()[0], rssi=rssi, org=org, stype=stype, channel=channel)
                            with self._lock_raw:
                                if mac in self._raw_data:
                                    self._raw_data[mac].append(record)
                                else:
                                    self._raw_data[mac] = [record]
            else:
                time.sleep(.1)

    def _filter_records(self):
        """
        Filter the records which are duplicated in a very short period of time.
        :return:
        """
        # TODO check logic and update if necessary
        while True:
            if not self._stop_event.is_set():
                with self._lock_raw:
                    for mac, record in self._raw_data.items()[:]:
                        t_list = [r['ts'] for r in record]
                        r_list = [r['rssi'] for r in record]
                        if time.time() - max(t_list) < 1.:
                            continue
                        r = dict(mac=mac,
                                 ts=t_list[len(t_list) // 2],
                                 rssi=round(np.average(r_list), 2),
                                 org=record[0]['org'],
                                 channel=record[0]['channel'])
                        self._raw_queue.put(r)
                        self._raw_data.pop(mac)
            time.sleep(1)

    def _update_buf(self):
        """
        Pop raw data from the queue and update the local buf in memory
        :return:
        """
        while True:
            if not self._stop_event.is_set():
                while self._raw_queue.qsize() > 0:
                    r = self._raw_queue.get()
                    r['distance'] = get_distance_from_rssi(r['rssi'])
                    if r['distance'] < config['Sniffing']['MaxSniffDistance']:
                        if config['UploadRaw']:
                            self._raw_out_queue.put(copy.deepcopy(r))
                        mac = r.pop('mac')
                        print('\t{} : MAC: {}, {}'.format(datetime.datetime.now(), mac, r))

                        # deprecated. data for visit filter
                        # with self._lock:
                        #     if mac in self._buf:
                        #         self._buf[mac]['record'].append(r)
                        #         self._buf[mac]['latest_ts'] = int(time.time())
                        #     else:
                        #         self._buf[mac] = {
                        #             'mac': mac,
                        #             'org': r['org'],
                        #             'start_ts': int(time.time()),
                        #             'latest_ts': int(time.time()),
                        #             'record': [r],
                        #         }
            time.sleep(1)

    def _channel_hop(self, built_in=True):
        """
        Pop raw data from the queue and update the local buf in memory
        :return:
        """
        channel = 0
        while True:
            channel = (channel % 13) + 1
            if not self._stop_event.is_set():
                with self._lock:
                    if built_in:
                        subprocess.call(shlex.split("nexutil -I mon.wlan0 --chanspec=%d" % channel))
                    else:
                        subprocess.call(shlex.split("iw dev wlan1 set channel %d" % channel))
            time.sleep(3)

    def _check_monitor(self):
        """
        Check monitoring interface.
        :return:
        """
        while True:
            if not self._stop_event.is_set():
                with self._lock:
                    pipe = os.popen('iwlist %s channel | grep Frequency' % SNIFF_INTERFACE)
                    msg = pipe.read().strip()
                    pipe.close()
                    if 'Channel' not in msg:
                        logger.error('>>>>>>> Monitoring failed. Rebooting')
                        time.sleep(5)
                        reboot_device()
            time.sleep(30 * 60)

    def _monitor_thread(self):
        while True:
            if not self._stop_event.is_set():
                with self._lock:
                    thread_list = threading.enumerate()
                    thread_names = [item.name for item in thread_list]
                    is_valid = all([t_name in thread_names
                                    for t_name in ['filter_rec', 'check_mon', 'update_buf', 'ping']])

                    if not is_valid:
                        logger.info(thread_list)
                        logger.error('>>>>> No ping thread. Rebooting ')
                        reboot_device()
                    logger.info(thread_list)
            time.sleep(5 * 60)

    def _ping_server(self):
        """
        Ping to server to notify the device is on.
        :return:
        """
        while True and cik_config['enabled']:
            if not self._stop_event.is_set():
                with self._lock:
                    try:
                        if check_internet_connection():
                            res = requests.post('%s/%s' % (config['HOST']['URL'], config['HOST']['PingEndpoint']),
                                                json=device_status(),
                                                headers=request_header())

                            if res.status_code == 200 and res.json()['code'] == 'success':
                                res_json = res.json()
                                # TODO update when backend is ready
                                if res_json['data']['needUpdate']['state']:
                                    update_device(res_json['data']['needUpdate']['file'])
                                elif res_json['data']['needReboot']['state']:
                                    reboot_by_flag()
                                elif res_json['data']['needUpdateList']['state']:
                                    update_blacklist(res_json['data']['needUpdateList']['file'])
                                logger.info('>>>>> Pinged server')
                            else:
                                logger.info('>>>>> Ping failed')
                        else:
                            logger.info('>>>>> No network connection')
                    except Exception as e:
                        logger.warn(str(e))
            time.sleep(5 * 60)

    def _extract_disappeared_devices(self):
        """
        Extract "disappeared" devices and put to the output queue
        :return:
        """
        while True:
            if not self._stop_event.is_set():
                with self._lock:
                    for k, v in self._buf.items()[:]:
                        if time.time() - v['latest_ts'] > config["Filtering"]['DisappearanceTime']:
                            self._buf.pop(k)
                            self._out_queue.put(v)
            time.sleep(1)

    def pause(self):
        self._stop_event.set()

    def resume(self):
        self._stop_event.clear()


if __name__ == '__main__':
    _q = multiprocessing.Queue()
    w = WiFiSniffer(out_queue=_q)
    w.start()
    while True:
        if _q.qsize() > 0:
            print(_q.get())
        time.sleep(.1)
